<?php

namespace Webinstart\Geolocator;

use Webinstart\Geolocator\Utility\Client;
use Webinstart\Geolocator\Entity\GeoRedirection;
use Webinstart\Geolocator\Repository\GeoRedirectionRepository;

class GeoRedirect
{
    private ?string $baseUrl = null;
    private ?string $endpoint = null;
    private ?string $apikey = null;
    private ?string $apiUrl = null;
    private string $lang = "EN";


    private array $data = [];

    /**
     * @return array|null
     */
    public function getData(?string $key = null)
    {
        if ($key) {
            if (array_key_exists($key, $this->data)) {
                return $this->data[$key];
            }
            return null;
        }
        return $this->data;
    }


    private ?Client $client;


    public function __construct()
    {
        $this->client = new Client();

        $this->baseUrl = $this->loadProviderEnvironnementBaseUrl();
        $this->endpoint = $this->loadEnpoint();

        $this->apikey = $this->getApiKey(); // to be set from WP backend
        $this->apiUrl = $this->buildUrl();
    }

    /**
     * This returns the base URL
     * @return string
     */
    private function loadProviderEnvironnementBaseUrl()
    {
        $apiProvider = "https://apis.webinify.dev";
        // $apiProvider = "https://apis.webinify.com";

        return $apiProvider;
    }


    /**
     * This returns the API key
     * @return string
     */
    private function getApiKey()
    {
        return  "b883303c-caf8-458d-bb31-81663b55bc8c";
    }

    /**
     * This returns the API endpoint
     * @return string
     */
    private function loadEnpoint()
    {
        return "/geolocation/v1/geolocate?key={API_KEY}&ip={IP}&lang={LANG}";
    }

    /**
     * This replaces the params in the url
     * @return string
     */
    private function replaceParams(string $url, array $params = [])
    {
        foreach ($params as $param => $value) {
            $url = str_replace($param, $value, $url);
        }
        return $url;
    }


    /**
     * This returns the full url of the APP
     * @return string
     */
    private function buildUrl()
    {

        $apiUrl = $this->replaceParams($this->baseUrl . $this->endpoint, [
            '{API_KEY}' => $this->apikey,
            '{IP}' => $this->client->getIp(),
            '{LANG}' => $this->lang
        ]); // dev

        return $apiUrl;
    }

    public function fetch()
    {
        if (function_exists('curl_init')) {

            //use cURL to fetch data
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->apiUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'geoPlugin PHP Class v1.1');
            $response = curl_exec($ch);
            curl_close($ch);
        } else if (ini_get('allow_url_fopen')) {

            //fall back to fopen()
            $response = file_get_contents($this->apiUrl, 'r');
        } else {
            trigger_error('geoPlugin class Error: Cannot retrieve data. Either compile PHP with cURL support or enable allow_url_fopen in php.ini ', E_USER_ERROR);
            return;
        }

        $this->mergeResponse($response);
        return $response;
    }


    private function mergeResponse($jsonResponse)
    {
        // {
        //     "id": 56,
        //     "data": {
        //         "currency": "USD",
        //         "lang": "en",
        //         "ip": "176.33.178.36",
        //         "city": "Istanbul",
        //         "region": "Istanbul",
        //         "regionCode": "34",
        //         "regionName": "Istanbul",
        //         "dmaCode": "",
        //         "countryCode": "TR",
        //         "countryName": "Turkey",
        //         "inEU": 0,
        //         "euVATrate": false,
        //         "continentCode": "AS",
        //         "continentName": "Asia",
        //         "latitude": "41.0145",
        //         "longitude": "28.9533",
        //         "locationAccuracyRadius": "1000",
        //         "timezone": "Europe/Istanbul",
        //         "currencyCode": "TRY",
        //         "currencySymbol": "&#89;&#84;&#76;",
        //         "currencyConverter": "20.2265"
        //     },
        //     "ipAddress": "176.33.178.36",
        //     "createdAt": "2023-05-30T09:02:27+00:00",
        //     "updatedAt": "2023-05-30T09:02:27+00:00"
        // }

        $res = json_decode($jsonResponse, true);
        $this->data = array_key_exists("data", $res) ? $res["data"] : null;
    }

    public function handleRedirection()
    {
        $geoRedirectionRepository = new GeoRedirectionRepository();
        $this->fetch();
        $countryCode = $this->getData("countryCode");

        // Define the cookie expiration time (1 year from now)
        $expiration = time() + 31536000;

        $redirections = $geoRedirectionRepository->findAll();

        $matchingGeoRedirection = array_filter(
            $redirections,
            function (GeoRedirection $geoRedirection) use ($countryCode) {
                return $geoRedirection->getLang() === $countryCode;
            }
        );

        if (!empty($matchingGeoRedirection)) {
            $matchingGeoRedirection = end($matchingGeoRedirection);

            // Set the language and country cookies
            setcookie('geo_language', $countryCode, $expiration, '/');
            setcookie('geo_country', $countryCode, $expiration, '/');

            // Set the redirected cookie
            setcookie('geo_redirected', 'true', $expiration, '/');

            wp_redirect($matchingGeoRedirection->getUrl());

            exit;
        }
    }
}

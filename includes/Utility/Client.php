<?php

namespace Webinstart\Geolocator\Utility;

class Client
{

    private ?string $ip = null;

    public function __construct()
    {
        $this->_loadIp();
    }

    /**
     * This returns current USER IP address
     * @return string
     */
    private function _loadIp()
    {
        global $_SERVER;

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $ip = apply_filters('wpb_get_ip', $ip);
        $this->setIp($ip);
        return $this;
    }

    /**
     * Get the value of ip
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set the value of ip
     *
     * @return  self
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }
}

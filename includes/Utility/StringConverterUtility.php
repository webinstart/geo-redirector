<?php

namespace Webinstart\Geolocator\Utility;

abstract class StringConverterUtility
{

    /**
     * Convert Camel Case String to underscore-separated
     * @param string $str The input string.
     * @param string $separator Separator, the default is underscore
     * @return string
     */
    public static function camelCase2UnderScore($str, $separator = "_")
    {
        if (empty($str)) {
            return $str;
        }
        $str = lcfirst($str);
        $str = preg_replace("/[A-Z]/", $separator . "$0", $str);
        return strtolower($str);
    }

    public static function underScoreToCamelCase($string, $capitalizeFirstCharacter = false)
    {

        $str = str_replace('_', '', ucwords($string, '_'));

        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }

        return $str;
    }
}

<?php

namespace Webinstart\Geolocator\Utility;

class CrawlerDetector
{
    public static function isGoogleCrawler()
    {
        global $_SERVER;
        $userAgent = $_SERVER['HTTP_USER_AGENT'];

        // Check if the user agent contains "Googlebot" or "Googlebot-Mobile"
        if (stripos($userAgent, 'Googlebot') !== false || stripos($userAgent, 'Googlebot-Mobile') !== false) {
            return true;
        }

        return false;
    }
}

<?php

namespace Webinstart\Geolocator;

use Webinstart\Geolocator\Entity\GeoRedirection;
use Webinstart\Geolocator\Fixtures\GeoRedirectionFixtures;
use Webinstart\Geolocator\GeoRedirect;
use Webinstart\Geolocator\Repository\GeoRedirectionRepository;
use Webinstart\Geolocator\Utility\CrawlerDetector;

class WebinstartGeolocatorPlugin
{
    private GeoRedirectionRepository $geoRedirectionRepository;

    public function __construct()
    {
        $this->geoRedirectionRepository = new GeoRedirectionRepository();
    }


    public static function init()
    {
        $plugin = new WebinstartGeolocatorPlugin();
        $plugin->run();
    }

    public static function onActivate()
    {
        global $wpdb;

        // Define the table name
        $table_name = $wpdb->prefix . GeoRedirection::TABLE;

        // Check if the table exists
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            // Table doesn't exist, create it
            $charset_collate = $wpdb->get_charset_collate();
            $sql = "CREATE TABLE $table_name (
                id INT(11) NOT NULL AUTO_INCREMENT,
                lang VARCHAR(255) NOT NULL,
                url VARCHAR(255) NOT NULL,
                is_active TINYINT(1) NOT NULL DEFAULT '1',
                PRIMARY KEY (id)
            ) $charset_collate;";
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);

            GeoRedirectionFixtures::initDatabase();
        }
    }


    public static function onUninstall()
    {
        global $wpdb;

        // Define the table name
        $table_name = $wpdb->prefix . GeoRedirection::TABLE;

        // Delete the table if it exists
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") == $table_name) {
            $wpdb->query("DROP TABLE IF EXISTS $table_name");
        }
    }

    /**
     * Network activate the plugin.
     */
    public static function network_activate() {
        global $wpdb;

        if (is_network_admin()) {
            // Create or update the database table
            // $this->create_table($wpdb->prefix);

            global $wpdb;

            // Define the table name
            $table_name = $wpdb->prefix . GeoRedirection::TABLE;
    
            // Check if the table exists
            if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
                // Table doesn't exist, create it
                $charset_collate = $wpdb->get_charset_collate();
                $sql = "CREATE TABLE $table_name (
                    id INT(11) NOT NULL AUTO_INCREMENT,
                    lang VARCHAR(255) NOT NULL,
                    url VARCHAR(255) NOT NULL,
                    is_active TINYINT(1) NOT NULL DEFAULT '1',
                    PRIMARY KEY (id)
                ) $charset_collate;";
                require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
                dbDelta($sql);
    
                GeoRedirectionFixtures::initDatabase();
            }
        }
    }

    /**
     * Network deactivate the plugin.
     */
    public static function network_deactivate() {
        global $wpdb;

        if (is_network_admin()) {
            // Delete the database table
            //$this->delete_table($wpdb->prefix);
        }
    }


    /**
     * Add the admin menu for the plugin.
     */
    public static function add_admin_menu()
    {
        // Add a top-level menu page
        add_menu_page(
            'GeoRedirection by Webinstart', // Page title
            'Geo redirections', // Menu title
            'manage_options', // Capability required to access the menu
            'webinstart-geo-redirector', // Menu slug
            array(self::class, 'admin_page'), // Callback function to render the page
            'dashicons-redo'
        );
    }

     /**
     * Add the network admin menu for the plugin.
     */
    public static function add_network_admin_menu() {
        add_menu_page(
            'GeoRedirection by Webinstart', // Page title
            'Geo redirections', // Menu title
            'manage_network', // Capability required to access the network admin menu
            'webinstart-geo-redirector', // Menu slug
            array(self::class, 'admin_page'), // Callback function to render the page
            'dashicons-redo'
        );
    }

    /**
     * Render the admin page.
     */
    public static function admin_page()
    {
        $geoRedirectionRepository = new GeoRedirectionRepository();
        // Check if the form is submitted
        if (isset($_POST['submit'])) {
            // Handle form submission
            if ($_POST['action'] === 'add') {
                // Add new entry to the database
                $object = new GeoRedirection(); // Create your object here
                $geoRedirectionRepository->save($object);
            } elseif ($_POST['action'] === 'delete') {
                // Delete entry from the database
                $object_id = $_POST['object_id'];
                $geoRedirectionRepository->remove($object_id);
            }
        }

        // Retrieve all entries from the database
        $entries = $geoRedirectionRepository->findAll();

        // Display the admin page content
        ?>
        <div class="wrap">
            <h1>Your Plugin</h1>

            <!-- Add new entry form -->
            <h2>Add New Entry</h2>
            <form method="post">
                <input type="hidden" name="action" value="add">
                <!-- Add your input fields for the new entry here -->
                <input type="submit" name="submit" value="Add">
            </form>

            <!-- Entries table -->
            <h2>Entries</h2>
            <table class="wp-list-table widefat fixed striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Lang</th>
                        <th>Redirect URL</th>
                        <th>Is active</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($entries as $entry) { ?>
                        <tr>
                            <td><?php echo $entry->getId(); ?></td>
                            <td><?php echo $entry->getLang(); ?></td>
                            <td><?php echo $entry->getUrl(); ?></td>
                            <td><?php echo $entry->getIsActive(); ?></td>
                            <td>
                                <a href="#" class="edit-entry" data-entry-id="<?php echo $entry->getId(); ?>">Edit</a> |
                                <form class="delete-entry-form" method="post" style="display: inline;">
                                    <input type="hidden" name="action" value="delete">
                                    <input type="hidden" name="object_id" value="<?php echo $entry->getId(); ?>">
                                    <input type="submit" name="submit" class="delete-entry-button" value="Delete">
                                </form>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>

        <script>
            // JavaScript/jQuery code for handling click events on the edit and delete buttons
            jQuery(document).ready(function($) {
                $('.edit-entry').click(function(event) {
                    event.preventDefault();
                    var entryId = $(this).data('entry-id');
                    // Handle the edit action, e.g., redirect to the edit page with the entry ID
                });

                $('.delete-entry-form').submit(function(event) {
                    event.preventDefault();
                    if (confirm('Are you sure you want to delete this entry?')) {
                        $(this).find('.delete-entry-button').attr('disabled', 'disabled');
                        this.submit();
                    }
                });
            });
        </script>
        <?php
    
    }


    private function isRunable()
    {
        // // Check if the user is in wp-admin
        if (is_admin()) {
            return false;
        }

        // Check if the user has already been redirected
        if (isset($_COOKIE['geo_redirected'])) {
            return false;
        }

        if (CrawlerDetector::isGoogleCrawler()) {
            return false;
        }
        return true;
    }

    public function run()
    {
        if ($this->isRunable() == false) {
            return;
        }

        // Get the current URL
        $current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        // Parse the URL
        $parsed_url = parse_url($current_url);
        // Get the path
        $url_path = $parsed_url['path'];

        if ($url_path == "/") {
            $geoPlugin = new GeoRedirect();
            $geoPlugin->handleRedirection();
        }
    }
}

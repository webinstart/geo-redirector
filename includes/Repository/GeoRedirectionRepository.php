<?php


namespace Webinstart\Geolocator\Repository;

use Webinstart\Geolocator\Entity\GeoRedirection;

class GeoRedirectionRepository
{
    private const TABLE = GeoRedirection::TABLE;


    public function save(GeoRedirection $object)
    {
        global $wpdb;

        // Define the table name
        $table_name = $wpdb->prefix . self::TABLE;

        // Prepare data for insertion
        $data = array(
            'lang' => $object->getLang(),
            'url' => $object->getUrl(),
        );

        // Insert the data into the database
        $result = $wpdb->insert($table_name, $data);

        if ($result === false) {
            // Error occurred during insertion
            return false;
        }

        // Return the ID of the saved object
        return $wpdb->insert_id;
    }

    public function remove($object_id)
    {
        global $wpdb;

        // Define the table name
        $table_name = $wpdb->prefix . self::TABLE;

        // Delete the object from the database
        $result = $wpdb->delete($table_name, array('id' => $object_id));

        if ($result === false) {
            // Error occurred during deletion
            return false;
        }

        // Return true if deletion was successful
        return true;
    }

    /**
     * @return GeoRedirection[]|null
     */
    public function findAll() {
        global $wpdb;

        // Define the table name
        $table_name = $wpdb->prefix . self::TABLE;

        // Query the database to retrieve all entries
        $query = "SELECT * FROM $table_name";
        $results = $wpdb->get_results($query, OBJECT);

        // Return the results
        return is_null($results) ? null : array_map(function($obj) {
            return GeoRedirection::fromArray($obj);
        }, $results);
    }

    /**
     * @return GeoRedirection[]|null
     */
    public static function getRedirections()
    {
        global $wpdb;

        // Define the table name
        $table_name = $wpdb->prefix . self::TABLE;

        // Query the database to retrieve all entries
        $query = "SELECT * FROM $table_name";
        $results = $wpdb->get_results($query, GeoRedirection::class);

        // Return the results
        return $results;
    }
}

<?php

namespace Webinstart\Geolocator\Fixtures;

use Webinstart\Geolocator\Entity\GeoRedirection;
use Webinstart\Geolocator\Repository\GeoRedirectionRepository;


class GeoRedirectionFixtures {

    public static function initDatabase()
    {
        $geoRedirectionRepository = new GeoRedirectionRepository();
        
        $objects = [
            GeoRedirection::fromArray(['lang' => 'TR', 'url' => "/tr/"]),
            GeoRedirection::fromArray(['lang' => 'FR', 'url' => "/fr/"]),
            GeoRedirection::fromArray(['lang' => 'BE', 'url' => "/be/"]),
            GeoRedirection::fromArray(['lang' => 'CH', 'url' => "/ch/"]),
            GeoRedirection::fromArray(['lang' => 'DE', 'url' => "/de/"]),
            GeoRedirection::fromArray(['lang' => 'AT', 'url' => "/at/"])
        ];
        
        foreach ($objects as $object) {
            $geoRedirectionRepository->save($object);
        }
    }
}
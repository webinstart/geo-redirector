<?php

namespace Webinstart\Geolocator\Entity;

use DateTimeInterface;
use Webinstart\Geolocator\Utility\StringConverterUtility;

class GeoRedirection
{
    public const TABLE = 'webinstart_geo_redirector';

    public static function fromArray($array)
    {
        $obj = new GeoRedirection();

        foreach($array as $key => $value) {
            $obj->__set($key, $value);
        }
       
        return $obj;
    }

    public function __set(string $name, mixed $value): void
	{
		if (method_exists($this, "set" . StringConverterUtility::underScoreToCamelCase($name, true))) {
			$this->{"set" . StringConverterUtility::underScoreToCamelCase($name, true)}($value);
		} else if (property_exists($this, StringConverterUtility::underScoreToCamelCase($name, false))) {
            $this->{StringConverterUtility::underScoreToCamelCase($name, false)} = $value;
		} else if (property_exists($this, $name)) {
			$this->{$name} = $value;
		}
	}


    private $id;

    private $lang = 'EN';

    private $url = "/en/";

    private $isActive = true;

    

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of lang
     */ 
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set the value of lang
     *
     * @return  self
     */ 
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get the value of url
     */ 
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of url
     *
     * @return  self
     */ 
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get the value of isActive
     */ 
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set the value of isActive
     *
     * @return  self
     */ 
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }
}
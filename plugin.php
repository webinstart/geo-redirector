<?php
/*
Plugin Name: Geo Redirect
Description: Detects user's country and language and redirects them to a different path based on their location
Plugin URI: https://webinstart.com/
Version: 1.0.0
Author: Webinstart
Author URI: https://webinstart.com/
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

require_once plugin_dir_path(__FILE__) . 'autoloader.php';

use Webinstart\Geolocator\WebinstartGeolocatorPlugin;

// Check if the plugin is network activated
if (is_multisite()) {
    // Add network activation and deactivation hooks
    add_action('network_activate_plugin', array(WebinstartGeolocatorPlugin::class, 'network_activate'));
    add_action('network_deactivate_plugin', array(WebinstartGeolocatorPlugin::class, 'network_deactivate'));
} else {
    // Add regular activation and deactivation hooks
    
    // Register activation hook
    register_activation_hook(__FILE__, array(WebinstartGeolocatorPlugin::class, 'onActivate'));
    // Register uninstall hook
    register_uninstall_hook(__FILE__, array(WebinstartGeolocatorPlugin::class, 'onUninstall'));
    // register_deactivation_hook(__FILE__, array(WebinstartGeolocatorPlugin::class, 'deactivate'));
}


add_action('init', array(WebinstartGeolocatorPlugin::class, 'init'));


// Check if the plugin is network activated
if (is_multisite() && is_network_admin()) {
    // Add network admin menu
    add_action('network_admin_menu', array(WebinstartGeolocatorPlugin::class, 'add_network_admin_menu'));
} else {
    // Hook the function to add the admin menu
    add_action('admin_menu', array(WebinstartGeolocatorPlugin::class, 'add_admin_menu'));
}

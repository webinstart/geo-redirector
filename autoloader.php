<?php


// Define the plugin root file path
define( 'WEBINSTART_GEO_LOCATOR_PATH', __FILE__ );


require_once plugin_dir_path(__FILE__) . 'includes/WebinstartGeolocatorPlugin.php';
require_once plugin_dir_path(__FILE__) . 'includes/GeoRedirect.php';

require_once plugin_dir_path(__FILE__) . 'includes/Utility/CrawlerDetector.php';
require_once plugin_dir_path(__FILE__) . 'includes/Utility/Client.php';
require_once plugin_dir_path(__FILE__) . 'includes/Utility/StringConverterUtility.php';


require_once plugin_dir_path(__FILE__) . 'includes/Repository/GeoRedirectionRepository.php';
require_once plugin_dir_path(__FILE__) . 'includes/Entity/GeoRedirection.php';

require_once plugin_dir_path(__FILE__) . 'includes/Fixtures/GeoRedirectionFixtures.php';

// // Register the autoloader
// spl_autoload_register( 'webinstart_geolocator_autoload' );

// /**
//  * Autoloader function for the plugin.
//  *
//  * @param string $class_name The class name to load.
//  */
// function webinstart_geolocator_autoload( $class_name ) {
//     $namespace = 'Webinstart\\GeoLocator\\';

//     // Check if the class belongs to the plugin
//     if ( strpos( $class_name, $namespace ) === 0 ) {
//         $class_name = str_replace( $namespace, '', $class_name );
//         $class_name = str_replace( '\\', '/', $class_name );
//         $file_path  = plugin_dir_path( WEBINSTART_GEO_LOCATOR_PATH ) . 'includes/' . $class_name . '.php';

//         if ( file_exists( $file_path ) ) {
//             require_once $file_path;
//         }
//     }
// }





